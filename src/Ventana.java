import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Ventana extends JFrame implements ActionListener{

	  private JLabel txt_Instrucciones; 
	  private JLabel txt_nombre;           
	    private JTextField caja_nombre;         
	    private JLabel txt_edad;           
	    private JTextField caja_edad; 
	    private JLabel txt_sexo;           
	    private JTextField caja_sexo;
	    private JLabel txt_peso;           
	    private JTextField caja_peso; 
	    private JLabel txt_altura;           
	    private JTextField caja_altura; 
	    private JButton boton;          
	
	
	public Ventana() {
		// TODO Auto-generated constructor stub
		 super();                   
	        configurarVentana();        
	        inicializarComponentes();   
	}

    private void configurarVentana() {
        this.setTitle("Salud Personal");                  
        this.setSize(310, 300);                                 
        this.setLocationRelativeTo(null);                       
        this.setLayout(null);                                 
        this.setResizable(false);                              
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    }
    
    private void inicializarComponentes() {
        // creamos los componentes
    	txt_Instrucciones= new JLabel();
        txt_nombre = new JLabel();
        caja_nombre = new JTextField();
        txt_edad = new JLabel();
        caja_edad = new JTextField();
        txt_sexo = new JLabel();
        caja_sexo = new JTextField();
        txt_peso = new JLabel();
        caja_peso = new JTextField();
        txt_altura = new JLabel();
        caja_altura = new JTextField();
        boton = new JButton();
        // configuramos lSs componentes
        txt_Instrucciones.setText("Ingrese los datos correspondientes ");
        txt_Instrucciones.setBounds(50, 20, 250, 25);   
        txt_nombre.setText("Nombre:");    
        txt_nombre.setBounds(50, 50, 100, 25);   
        caja_nombre.setBounds(150, 50, 100, 25);   
        txt_edad.setText(" Edad:");    
        txt_edad.setBounds(50, 80, 100, 25);   
        caja_edad.setBounds(150, 80, 100, 25);   
        txt_sexo.setText(" Sexo:");    
        txt_sexo.setBounds(50, 110, 100, 25);   
        caja_sexo.setBounds(150, 110, 100, 25);   
        txt_peso.setText(" Peso:");    
        txt_peso.setBounds(50, 140, 100, 25);   
        caja_peso.setBounds(150, 140, 100, 25);   
        txt_altura.setText("Altura:");    
        txt_altura.setBounds(50, 170, 100, 25);   
        caja_altura.setBounds(150, 170, 100, 25);   
        boton.setText("Generar");   
        boton.setBounds(50, 210, 200, 30);  
        boton.addActionListener(this);      
        
        // adicionamos los componentes a la ventana
        this.add(txt_Instrucciones);
        this.add(txt_nombre);
        this.add(caja_nombre);
        this.add(txt_edad);
        this.add(caja_edad);
        this.add(txt_sexo);
        this.add(caja_sexo);
        this.add(txt_peso);
        this.add(caja_peso);
        this.add(txt_altura);
        this.add(caja_altura);
        this.add(boton);
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if (caja_nombre.getText().equals("")||caja_sexo.getText().equals("")|| caja_edad.getText().equals("")||caja_peso.getText().equals("")||caja_altura.getText().equals("")) {
			
			  JOptionPane.showMessageDialog(this,"Ingrese todos los datos Solicitados","Warning",JOptionPane.WARNING_MESSAGE);
		}else {
			
		
		
		  String nombre = caja_nombre.getText();  
		  char sex = caja_sexo.getText().charAt(0);
		  int edad=Integer.parseInt(caja_edad.getText());
		  BigDecimal peso = new BigDecimal (caja_peso.getText());
		  Double altura =Double.parseDouble(caja_altura.getText());
		  
		    Persona pers = new Persona (caja_nombre.getText(),edad,"",sex, peso,altura);
		    System.out.println (pers.toString());
		    System.out.println ("Es mayor de edad: "+pers.esMayorDeEdad()); 
		    System.out.println ("Sexo correcto: "+pers.comprobarSexo(sex)); 
		    pers.calcularIMC();
		    pers.esMayorDeEdad();
		    
		    
		   // MensajePeso
		  JOptionPane.showMessageDialog(this, "<html>Hola " + nombre + "<br>" + "*"+ pers.MensajePeso +"<br>"+"* "+pers.MensajeEdad+"<br><br><br>"+"Datos Ingresados:"+"<br>"
				  						+"Nombre: "+ nombre +"<br>" +"Edad: "+edad+"<br>"+"Sexo: "+sex+"<br>"+"Peso: "+peso+"<br>"+"Altura: "+altura+"</html>");
	}
	
	}

}
