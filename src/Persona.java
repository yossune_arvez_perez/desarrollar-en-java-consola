import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class Persona {
	
	private String nombre ="" ;
	private int edad = 0;
	private String NSS ;
	private char sexo  ='H';
	private BigDecimal peso = BigDecimal.ZERO;
	private  Double altura =(double) 0;
	
	private  String mensajeFinal ="";
	final static int Falta_de_peso = -1;
	final static int Peso_normal = 0;
	final static int Sobrepeso =1;
	private int PesoIdeal;
	public String MensajePeso="";
	public String MensajeEdad="";


	
	public Persona (String nombre,int edad,String NSS,char sexo, BigDecimal peso, Double altura ) {
	
		this.nombre=nombre;
		this.edad =edad; 
	
		this.sexo=sexo;
		this.peso=peso;
		this.altura = altura;
		
		generaNSS();
			
	
		
	}
	
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}


	public void setAltura(Double altura) {
		this.altura = altura;
	}


	public int	calcularIMC() {
		BigDecimal   alturaMult = new BigDecimal (Math.pow(altura, 2));
		BigDecimal  pesofinal= (peso.divide(alturaMult, 2, RoundingMode.HALF_UP ));
		int resultado =0; 
		
		char sexSeleccionado =Character.toUpperCase(sexo);
		switch(sexSeleccionado) {
		  case 'H':
		 
			  if (pesofinal.compareTo(new BigDecimal(20))==-1) {
				  //peso bajo
				  resultado=Falta_de_peso;
				  MensajePeso="Tiene Bajo peso";
			  }else if (pesofinal.compareTo(new BigDecimal(25))==1) {
					  //peso alto
					  resultado=Sobrepeso; 
					  MensajePeso="Tiene sobrepeso";
				  }
				  else {
					  //peso ideal
					  resultado=Peso_normal;
					  MensajePeso="Tiene un Peso Ideal";
				  }
			  
		    break;
		  case 'M':
		  
			  if (pesofinal.compareTo(new BigDecimal(19))==-1) {
				  //peso bajo
				  resultado=Falta_de_peso;
				  MensajePeso="Tiene bajo peso";
			  }else 
			  {
				  
				  if (pesofinal.compareTo(new BigDecimal(24))==1) {
					  //peso alto
					  resultado=Sobrepeso; 
					  MensajePeso="Tiene sobrepeso";
				  }
				  else {
					  //peso ideal
					  resultado=Peso_normal;
					  MensajePeso="Tiene un Peso Ideal";
				  }
				  
			  }
			  
			  
		    break;
		default:
			MensajePeso="no se puede generar su peso por por el tipo de sexo que ingreso";
			break;
		   
		}
		System.out.println("Peso: "+resultado);
		return resultado;
	}
	
	
	public boolean	esMayorDeEdad() {
		boolean respuesta =false;
		if (edad>18) {
			respuesta= true;
			MensajeEdad= "Eres mayor de Edad";
		}
		else {
			MensajeEdad= "Eres menor de Edad";
		}
		
		return respuesta;
	}
	

	public boolean	comprobarSexo(char sexo) {
	
		boolean respuesta = false; 
		char sexSeleccionado =Character.toUpperCase(sexo);
		
		if (sexSeleccionado=='H'|| sexSeleccionado=='M') {
			
			respuesta=true;
			
		}
		//System.out.println(respuesta);
		return respuesta;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", NSS=" + NSS + ", sexo=" + sexo + ", peso=" + peso
				+ ", altura=" + altura + "]";
	}

	private void generaNSS() {
		
		String val = "" ; 
		Random random = new Random();
		for(int i = 0; i < 8; i++)
		{
			String charOrNum = random.nextInt (2)% 2 == 0? "Char": "num"; 
				
			 if ("char" .equalsIgnoreCase (charOrNum)) 
			{
				 int choice = random.nextInt (2)% 2 == 0? 65: 97; 
				val += (char) (choice + random.nextInt(26));
			}
			  if ("num" .equalsIgnoreCase (charOrNum))
			{
				val += String.valueOf(random.nextInt(10));
			}
		}
		     NSS=val;
		//	System.out.println(val);
        
         
	}
	

	
}
